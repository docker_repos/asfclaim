# ASFclaim
GitLab: [https://gitlab.com/docker_repos/asfclaim](https://gitlab.com/docker_repos/asfclaim)  
DockerHub: [https://hub.docker.com/r/mega349/asfclaim](https://hub.docker.com/r/mega349/asfclaim)  
Fork from: [C4illin/ASFclaim](https://github.com/C4illin/ASFclaim/) [SHA:[d1e6e2ce2aa62b147aa4878063d0076091fcb258](https://github.com/C4illin/ASFclaim/tree/d1e6e2ce2aa62b147aa4878063d0076091fcb258)]
## Automatically claims new free packages on [Steam](https://store.steampowered.com/) if available
### Needs [ArchiSteamFarm](https://github.com/JustArchiNET/ArchiSteamFarm) with IPC enabled

---

## Discord Webhook (if enabled):
### **A successfully claimed Game:**
![](https://gitlab.com/docker_repos/asfclaim/-/raw/main/resources/readme/app_game_status.png)
### **A claimed package containing a DLC, with or without status if its hidden:**
![](https://gitlab.com/docker_repos/asfclaim/-/raw/main/resources/readme/sub_dlc_status.png)
![](https://gitlab.com/docker_repos/asfclaim/-/raw/main/resources/readme/sub_dlc_no-status.png)  
### **A claimed game package with multiple results and visible botnames:**
Status 'OK' is not really OK, because the game is no longer available, not released yet or something else...  
![](https://gitlab.com/docker_repos/asfclaim/-/raw/main/resources/readme/app_game_status-long.png)  
If the status is hidden (`WEBHOOK_SHOWACCOUNTSTATUS=false`) no botnames will be exposed and no status will be displayed.  
This could be usefull for public discord channels.

---

## Installation:
### compose.yml
```yaml
version: '3.8'

volumes:
    data:

services:
    asfclaim:
        image: mega349/asfclaim:latest
        environment:
            - ASF_PROTOCOL=http
            - ASF_HOST=localhost
            - ASF_PORT=1242 
            - ASF_PASS=secret # need to be in plaintext and WILL be exposed to your container log!
            - ASFCLAIM_INTERVAL=6
            - WEBHOOK_URL=none # replace with your discord webhook URL
            - WEBHOOK_ENABLEDTYPES=error;warn;success # 'info' is maybe too much
            - WEBHOOK_SHOWACCOUNTSTATUS=true # set to 'false' if you dont like your botnames exposed to discord (see above)
        volumes:
            - data:/app/storage/
```

### Environment vars:
| ENV                         | Description                                 | Info              | default value             | NEEDED  |
| --------------------------- | ------------------------------------------- | ----------------- | ------------------------- | ------- |
| `ASF_PROTOCOL`              | ASF-IPC Transfer protocol                   | `http` or `https` | `http`                    | No      |
| `ASF_HOST`                  | ASF-IPC Hostname or IP                      |                   | `localhost`               | **Yes** |
| `ASF_PORT`                  | ASF-IPC Port                                |                   | `1242`                    | No      |
| `ASF_PASS`                  | ASF-IPC Password                            | Plaintext         | `secret`                  | **Yes** |
| `ASFCLAIM_INTERVAL`         | Hours to wait for execution                 |                   | `6`                       | No      |
| `WEBHOOK_URL`               | Discord Webhook Url                         | `none` = disabled | `none`                    | No      |
| `WEBHOOK_ENABLEDTYPES`      | Displayed notification typs in Discord chat | ";" - seperated   | `error;warn;info;success` | No      |
| `WEBHOOK_SHOWACCOUNTSTATUS` | Show result from ASF                        | `true` or `false` | `true`                    | No      |

## Planed:
- [ ] Try activating base games for free DLCs
- [ ] Per user progress
- [ ] Package queue
- [ ] Use more than 10 packages, up to 50 per hour (steam limit)
- [ ] Changeable package source
- [ ] Filter to ignore (Demos, Videos, ...) or to just activate free promotions (100% discount)

## External resources:
- Webhook Bot Icon: [GitHub:ArchiSteamFarm](https://raw.githubusercontent.com/JustArchiNET/ArchiSteamFarm/main/resources/ASF_512x512.png)
- Webhook Placeholder image: [placeholder.com](https://via.placeholder.com/460x215.jpg?text=Cant+load+image)
- Claimable package list: [Github:C4illin:gist](https://gist.github.com/C4illin/e8c5cf365d816f2640242bf01d8d3675)
- Steam API
