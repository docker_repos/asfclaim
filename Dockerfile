ARG nodeversion

FROM node:${nodeversion}

RUN mkdir /app
COPY ./app/ /app/
RUN mkdir /app/storage
RUN chown -R node:node /app

ENV ASF_PROTOCOL=http
ENV ASF_HOST=localhost
ENV ASF_PORT=1242
ENV ASF_PASS=secret
ENV ASFCLAIM_INTERVAL=6
ENV WEBHOOK_URL=none
ENV WEBHOOK_ENABLEDTYPES=error;warn;info;success
ENV WEBHOOK_SHOWACCOUNTSTATUS=true

USER node
WORKDIR /app
ENTRYPOINT ["sh", "./run.sh"]
